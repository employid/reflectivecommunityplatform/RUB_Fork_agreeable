jQuery(document).ready(function($) {

	$(document).on("DOMSubtreeModified", function(){
		$('.ag-open-popup-link').magnificPopup({
		    type:'inline',
		    midClick: true,
			closeBtnInside: true,
            showCloseBtn: true,
            fixedContentPos: true
		});
	});
	
	$(document).ready(function(){
		$('.ag-open-popup-link').magnificPopup({
		    type:'inline',
		    midClick: true,
            closeBtnInside: true,
            showCloseBtn: true,
            fixedContentPos: true
		});
	});

    $('#rub_mfp_close').click(function(){
        $.magnificPopup.close();
    });

	if($('.woocommerce .login')) {
        $(".woocommerce>#agreeable_login_field").insertBefore(".woocommerce .login #rememberme");
	}
			
});